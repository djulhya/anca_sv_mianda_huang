/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Eurosab
 */
public class ListJoueur {
    static int numJoueur = 0;
    private Map<Integer , Joueur> mapJoueur = new HashMap<>();
    
    public ListJoueur()
    {
    }
    
    public synchronized int nouveauJoeur()
    {
        numJoueur+=1;
        Joueur j = new Joueur();
        j.setNumJoueur(numJoueur);
        this.mapJoueur.put(numJoueur, j);
        return numJoueur;
    }
    
    public synchronized void removeJoeur(int i)
    {
        if(this.mapJoueur.containsKey(i))
            this.mapJoueur.remove(i);
    }
    
    public synchronized Joueur getJoueur(int i)
    {
        return this.mapJoueur.get(i);
    }

    public synchronized Map<Integer, Joueur> getJoueurMap() {
        return this.mapJoueur;
    }

    public synchronized long getJoueurLastSessionTime(Integer i) {
        return this.mapJoueur.get(i).getLastSessionTime();
    }
    
    /*public boolean JoueurExist(String pseudo)
    {
        boolean res = false;
        System.out.println("nombre de joueur dans map : "+this.mapJoueur.size());
        for(int x = 0; x < this.mapJoueur.size();++x)
        {
            Joueur j = new Joueur();
            j = this.getJoueur(0);
            if(j!=null)
            {
                if(j.getIdJoueur()==pseudo)
                {
                    return true;
                }
            }
            else
                System.out.println("erreur accès map");
        }
        
        return res;
    }*/
    

}
