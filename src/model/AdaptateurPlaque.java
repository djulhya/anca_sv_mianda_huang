/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.PlateLib;

/**
 *
 * @author 2605glmiandambuyi
 */
public class AdaptateurPlaque {
    
    PlateLib p;
    public AdaptateurPlaque(PlateLib pl)
    {
        p = pl;
    }
    
    public int getPlaque()
    {
        return p.value();
    }
    
}
