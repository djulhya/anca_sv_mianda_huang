/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Eurosab
 */
public class ListJoueurFactory {
    private static ListJoueur listJoueur;
    
    public static synchronized ListJoueur getListJoueur() {
        if (listJoueur == null) {
            listJoueur = new ListJoueur();
        }
        return listJoueur;
    }
    
}
