
package model;

import java.io.Serializable;
import java.util.Calendar;

/**
 *
 * @author xun
 */
public class Resultat implements Serializable{
    private Solution solution;
    private int butPropose;
    private int but;
    private long timeStart;
    private long timeEnd;
     private int numJoeur;
    private Joueur jr;
    
    public Resultat (Solution s, int butp , int but, long tempsDeb 
            , Joueur j)
    {
        this.solution=s;
        this.butPropose=butp;
        this.but = but;
        this.timeStart=tempsDeb;
        this.jr = j;
        
    }
    
    public Solution getsolution(){
        return this.solution;
    }
    
    public int getButPropose(){
        return this.butPropose;
    }
    
    public int getBut()
    {
        return this.but;
    }
    public int getNumJoeur()
    {
        return this.numJoeur;
    }
    
    public Joueur getJoueur()
    {
        return this.jr;
    }
    public long getTimeStart(){
        return this.timeStart;
    }
    
    public void setTimeEnd(){
        this.timeEnd=Calendar.getInstance().getTimeInMillis();
    }

    public long getTimeEnd(){
        return this.timeEnd;
    }
}
