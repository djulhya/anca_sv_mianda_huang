    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import solver.SolverLibFactory;
import interfaces.*;

/**
 *
 * @author xun
 */
public class Play implements Serializable{
    private int numJeu;
    private Config config=Config.CONF1;
    
    private But but;
    private int butPropose;
    private Expression expression;
    private Solution solution;
    private ListPlaques listPlaque ;
    
    private long timeStart;
    private long timeEnd;
    private long duree;
    private int score;
    private String message;
    private String jouer = "ko";

    public Play()
    {
        this.butPropose = 0;
        this.expression = new Expression();
        this.solution = new Solution();
    }
    
    public Play(int numJeu , Config c)
    //creer un nouveau jeu avec le numero de jeu distribué et le configuration choisi par client
    {
        
        GameLibGenerator gameGen = SolverLibFactory.getGameLibGenerator();
        GameLib game = null;
        switch (c) 
        {
            case CONF1:
                game = gameGen.easyGameLib();
                break;
            case CONF2:
                game = gameGen.mediumGameLib();
                break;
            case CONF3:
                game = gameGen.difficultGameLib();
                break;
            case CONF4:
                game = gameGen.withoutExactSolutionGameLib();
                break;
            case CONF5:
                game = gameGen.withExactSolutionsGameLib();
                break;
            default:
                game = gameGen.randomGameLib();
                break;
                    
        }
        
        AdaptateurPlay jeu = new AdaptateurPlay(game);
        
        this.but = new But(jeu.getBut());
        this.listPlaque = new ListPlaques(jeu.getListPlaque());
        
        //this.but = new But(c);
        //this.listPlaque = new ListPlaques(c);
        
        this.timeStart = Calendar.getInstance().getTimeInMillis();
        this.numJeu = numJeu;
        this.setDuree(c);
        
    }
   
    public long getTimeStart(){ return this.timeStart;}
    
    public void setTimeStart(long startTime){ this.timeStart=startTime;}
    
    public int getNumJeu(){ return this.numJeu;}
    
    public long getDuree(){return this.duree;}
    
    private void setDuree(Config c) {
        /*switch (c) {
            case CONF2:
                this.duree = 120000;break;
            case CONF3:
                this.duree = 60000; break;
            case CONF1:
            case DEFAULT:
                this.duree = 180000;
        }*/
        this.duree = 180000;
    }
    
    public void setSolution(Solution s)
    {
        this.solution = s;
    }
    public boolean timeOut(long timeStart, long timeEnd , long tmp){
        return timeEnd>=(timeStart+tmp)+1000;
    }
    public int getConfig(){
        return this.config.ordinal();
    }
   
    public void nouvellePartie(But b ,ListPlaques lp, long startTime )
    {
        this.but = b;
        this.butPropose = -1;
        this.listPlaque = lp;
        this.solution.initSolution();
        this.timeStart=startTime;
    }
    
    public But getButB(){ return this.but;}
    
    public int getButInt(){return this.but.getBut();}
    
    public int getButPropose(){ return this.butPropose;}
    
    public void setButPropose(int x){ this.butPropose = x;}
    
    public ListPlaques getlistPlaque(){return this.listPlaque;}
    
    public ListPlaques getPlaque(){return this.listPlaque;}
    
    public String getJouer()
    {
        return this.jouer;
    }
    
    public void setJouer()
    {
        this.jouer = "ok";
    }
    
    public String getMessage()
    {
        return this.message;
    }
    
    public int getScore()
    {
        return this.score;
    }
    
    public boolean verifierPlaque(int x)
    { return this.listPlaque.EstUnePlaque(x);}
    
    public boolean doublePlaque(int x )
    {return this.listPlaque.doublePlaque(x);}
    
    public void ajouterPlaque(int x)
    {this.listPlaque.ajouterPlaque(x);}
    
    public void supprimerPlaque(int x)
    {this.listPlaque.retirerPlaque(x);}
    
    public boolean verifierEprssion(int x , int y , Operation o , int t)
    {
        this.expression = new Expression(x,y,t,o);
        return this.expression.verifierExpression();
    }
    
    public void ajouterExpression(int x , Operation o , int y , int t)
    {
        this.expression = new Expression(x,y,t,o);
        this.solution.addExpression(expression);
    }
    
    public void ajouterExpression(int x , String o , int y , int t)
    {
        Operation op ;
        
        if(o.equals("+"))
            op=Operation.addition;
        else if(o.equals("-"))
            op= Operation.soustraction;
        else if(o.equals("*"))
            op = Operation.multiplication;
        else
            op = Operation.division;
        this.ajouterExpression(x, op, y, t);
    }
  
    public int getResultat()
    {return this.solution.getResultat();}
    
    public boolean verifierResultat()
    {
        boolean res = false;
        if(this.butPropose==this.solution.getResultat()||this.but.getBut() 
                == this.solution.getResultat())
            res = true;
        return res;
    }

    public int getTotal()
    {return this.expression.getTotal();}
   
    public Solution getSolution(){return this.solution;}
    
    public void setScore(int x)
    {
        this.score = x;
    }
    
    public void setMessage(String s)
    {
        this.message = s;
    }
    
    private String configuration()
    {
        String str ="";
        if(this.config == Config.CONF2)
        {
            str +="moyen";
        }
        else if(this.config == Config.CONF3)
        {
            str += "difficile";
            
        }
        else
            str+= "facile";
        
        return str;
    }
    
    public void afficherPlay()
    {
        System.out.println("Numéro du Jeu : " + this.numJeu);
        System.out.println("Configuration : " + this.configuration());
        System.out.println("But : " +this.but.getBut());
        System.out.println("Plaques : " + this.listPlaque.toString());
        System.out.println("But proposé : "+ this.butPropose);
        System.out.println("Expressions : ");
        for(Expression e : this.solution.getListExpression())
        {
            System.out.println(e.toString());
        }
        System.out.println("Resultat :" + this.message);
        System.out.println("Score : " + this.score);
    }
   
    public List<Solution> bestSolutions() {
//Méthode permettant de trouver les meilleures solutions pour le jeu fourni en paramètre.
        SolverLib sl = SolverLibFactory.getSolverLib();
        AdaptateurGameLib ag= new AdaptateurGameLib(this);
        List<SolutionLib> sls= sl.bestSolutions(ag);
        List<Solution> sol= new AdaptateurSolution().getListSolution(sls);
        return sol;
    }
 
    public boolean checkSolution(Solution solution) {
//Méthode permettant de vérifier si la solution est une solution valide pour le jeu.
        SolverLib sl = SolverLibFactory.getSolverLib();
        System.out.println("constructor solverlib");
        AdaptateurGameLib ag= new AdaptateurGameLib(this);
        System.out.println("AdaptateurGameLib");
        AdaptateurSolutionLib as = new AdaptateurSolutionLib(solution);
        System.out.println("AdaptateurSolutionLib");
        System.out.println("resultat check"+sl.checkSolution(ag, as));
        return sl.checkSolution(ag, as);
    }
 
    public List<Solution> exactSolutions(){ 
//Méthode permettant de trouver les solutions exactes pour le jeu fourni en paramètre. 
        SolverLib sl = SolverLibFactory.getSolverLib();
        AdaptateurGameLib ag = new AdaptateurGameLib(this);
        List<SolutionLib> sls = sl.exactSolutions(ag);
        List<Solution> sol= new AdaptateurSolution().getListSolution(sls);
        return sol;
    }

}
