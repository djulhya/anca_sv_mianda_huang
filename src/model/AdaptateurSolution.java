/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.ExpressionLib;
import interfaces.SolutionLib;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author localwsp
 */
public class AdaptateurSolution extends Solution{
    
    private SolutionLib sla;
    private Solution sol;

    
    public AdaptateurSolution(){}
    
    public List<Solution> getListSolution(List<SolutionLib> lsl){
        List<Solution> listSolution= new ArrayList<>(); 
        int x = 0;
        for(SolutionLib sl : lsl)
        {
            //list de solution add une nouvelle solution
            
            if(!(sl.getExpressions().size()==1 && sl.getExpressions().get(0).isUnary()))
            {
                Solution s= getSolution(sl);
                listSolution.add(s);
                ++x;
            }
        }
        System.out.println("x = "+x);
        return listSolution;
    }
    
    public Solution getSolution(SolutionLib slc){
        Solution s = new Solution();
        for (ExpressionLib el: slc.getExpressions()){
            AdaptateurExpression ae = new AdaptateurExpression(el);
            s.addExpressionAdaptateur(ae);
        }
        System.out.println("solution: "+s.toString());
        return s;
    
    }
    
    
}
