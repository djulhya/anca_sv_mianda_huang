/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.GameLib;
import interfaces.PlateLib;


/**
 *
 * @author 2605glmiandambuyi
 */
public class AdaptateurGameLib implements GameLib{

    Play jeu;
    
    AdaptateurGameLib(Play game)
    {
       this.jeu = game;
    }
    @Override
    public int goal() {
       
        return this.jeu.getButInt();
    }

    @Override
    public PlateLib[] gamePlates() {
        PlateLib[] listPlateLib = new PlateLib[6];
        int i = 0;
        for(Plaques p :  this.jeu.getlistPlaque().listPlaqueSelect)
        {
            AdaptateurPlateLib adaptPlate = new AdaptateurPlateLib(p);
            listPlateLib[i] = adaptPlate;
            ++i;
        }
        return listPlateLib;
    }
    
}
