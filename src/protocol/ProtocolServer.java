

package protocol;

import commun.Message;
import commun.Protocol;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 *
 * @author xun
 */
public class ProtocolServer extends Protocol{
    
     public ProtocolServer(Socket s) throws IOException {
        this.socket = s;
        this.outObject = new ObjectOutputStream( socket.getOutputStream());
        this.inObject = new ObjectInputStream(socket.getInputStream());
    }
    
     private int connexionJeu() {
         try{
             String s = this.inObject.readObject().toString();
             if(s.equals("RQST"))
             {
                write("ACK");
                return CONN_ACK;
             } 
             else 
             {
                return CONN_NOT_JEU;
             }  
        } catch (Exception ex) {
            System.out.println(" PAS CONNECTE");
            return CONN_KO;
        }
    }
     
     private Message attendreMsg() {
        try {
            Message m = (Message)inObject.readObject();
            msg_recu = new Message(m);
            write("ACK");
            return msg_recu;
        } 
        catch (Exception e) 
        {
            System.out.println(e.toString());
            return null;
        }
    }

    public Message wait_message() {
        Message msg = null;
        if (connexionJeu() == CONN_ACK) {
            msg = attendreMsg();
        }
        else
        {
            System.out.println("connection jeu KO");
        }
       
        return msg;
    }

    public int respond(Message reponse) {
        write(reponse);
        
        try{
            String s = this.inObject.readObject().toString();
             if(s.equals("ACK"))
             {
                return CONN_ACK;
             } 
             else 
             {
                return CONN_NOT_JEU;
             }
              
        } catch (Exception ex) {
            return CONN_KO;
        }
    }
}
