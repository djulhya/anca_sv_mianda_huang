/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controleur;

import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import model.Joueur;
import model.ListJoueur;
import model.ListJoueurFactory;

/**
 *
 * @author xun
 */
public class ThreadSession {
    Map<Integer , Joueur> mapJoueur = new HashMap<>();
    ListJoueur listJoueur = new ListJoueur();
    Set<Integer> setJoueurSessionOut= new HashSet<>();
    Timer sessionJoueur = new Timer();
    long delaisSession=300000;
    long timeActu=0;
    
    public ThreadSession(){
        
        sessionJoueur.schedule(new TimerTask(){
                @Override
                public void run() {
                    
                        listJoueur = ListJoueurFactory.getListJoueur();
                        mapJoueur =  listJoueur.getJoueurMap();

                        timeActu = Calendar.getInstance().getTimeInMillis();
                        synchronized(mapJoueur){
                            if(mapJoueur.isEmpty()) {}
                            else{
                                  for(Integer i:mapJoueur.keySet())
                                  {
                                      if(timeActu-listJoueur.getJoueurLastSessionTime(i)>delaisSession)
                                      {/*
                                          synchronized (listJoueur){
                                              listJoueur.removeJoeur(i);
                                          }*/
                                          setJoueurSessionOut.add(i);
                                      }

                                  }
                             }
                        }
                        
                        if(setJoueurSessionOut.isEmpty()){}
                        else
                        {
                            synchronized (listJoueur){
                                for(Integer in: setJoueurSessionOut){
                                    listJoueur.removeJoeur(in);
                                }
                            }
                        }
                    }
                
                
            },0,delaisSession);
    }
    //à refaire voir la référence suivante
    //http://www.journaldunet.com/developpeur/tutoriel/jav/050623-java-repetition-timer-timertask.shtml
}

