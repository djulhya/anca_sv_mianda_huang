/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controleur;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import protocol.ProtocolServer;

/**
 *
 * @author xun
 */
public class CtrlServer {
    
   private int port= 8189;
    public CtrlServer(){}
    
    public CtrlServer(int port) {
        this.port = port;
    }
    
    public void start() 
    {
        int nbconn = 0;
        try {
            InetAddress adresse = InetAddress.getLocalHost();
            System.out.println("Adresse: " + adresse);
            ServerSocket server = new ServerSocket(port);
            ExecutorService executor = Executors.newCachedThreadPool();
            ThreadSession thrSession = new ThreadSession();
            while (true) 
            {
                System.out.println("Connexion(s) établie(s) : " + nbconn);
                Socket s = server.accept();
                nbconn++;
                ProtocolServer protocolServ = new ProtocolServer(s);
                
                //ThreadSession thrSession = new ThreadSession();
                
                ThreadCtrl thrClient = new ThreadCtrl(protocolServ);
                executor.execute(thrClient);
             }
        } catch (IOException e) {
            System.out.println("Erreur: " + e);
        }
    }
  
}
