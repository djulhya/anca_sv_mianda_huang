

package controleur;

import commun.Message;
import commun.Protocol;
import static commun.Protocol.CONN_ACK;
import java.util.Calendar;
import model.*;
import protocol.ProtocolServer;

/**
 *
 * @author xun
 */
public class ThreadCtrl implements Runnable{

    private ProtocolServer protocolServer;
    Message demande;
    Play play;
    private static int nbJeu = 0;
    ListJoueur listJoueur;
    
    
    public ThreadCtrl(ProtocolServer p) {
        protocolServer = p;
        this.listJoueur = ListJoueurFactory.getListJoueur();
    }
    
    @Override
    public void run() {
        demande = protocolServer.wait_message();
        System.out.println("message du client : "+this.demande.getCode());
        traiter_msg();
      
    }
    
    private void traiter_msg() 
    {
        if (demande == null) {
            aff("Problème de protocole");
            return;
        }
        switch (demande.getCode()) {
            
            //connexion et debut
            case Message.OP_NOUVEAU_JOEUR:
                this.creerJoeur((int)demande.getObject());
                break;
            case Message.OP_DEB:
               this.creerJeu((Joueur)demande.getObject()); 
               break;   
            case Message.OP_INSCRIPTION:
                this.inscription((Joueur)demande.getObject());
                break;
            case Message.OP_CONNEXION:
                this.connexion((Joueur)demande.getObject());
                break;
            default:
                break;
              
                
            //configuration
            case Message.OP_CONFIG_DEFAULT:
                this.changerConfiguration(0, (Joueur)demande.getObject());
                break;
            case Message.OP_CONFIG_FACILE:
                this.changerConfiguration(1, (Joueur)demande.getObject());
                break;
            case Message.OP_CONFIG_MOYEN:
                this.changerConfiguration(2, (Joueur)demande.getObject());
                break;
            case Message.OP_CONFIG_DIFFICILE:
                this.changerConfiguration(3, (Joueur)demande.getObject());
                break;
            case Message.OP_CONFIG_NO_SOLUTION:
                this.changerConfiguration(4, (Joueur)demande.getObject());
                break;
            case Message.OP_CONFIG_SOLUTION:
                this.changerConfiguration(5, (Joueur)demande.getObject());
                break;
            
            //affichage
                
            case Message.BEST_SOLUTION:
                this.envoyerBestSolution((Joueur)demande.getObject());
                break;  
            case Message.LISTE_PLAY: //pour le joueur anonyme
                this.envoyerListePlay((Joueur)demande.getObject());
                break;
            case Message.LISTE_PLAY_DATE :
                // liste des parties selon id_session donnée par joueur
                //retour une liste string
                this.envoyerListPlayDate((Joueur)demande.getObject());
                break;
            case Message.LISTE_SESSION_DATE :
                //list de tous sessions avec date d'un joueur
                //retour une liste de string
                this.envoyerListSessionDate((Joueur)demande.getObject());
                break;
            case Message.LISTE_PLAY_CONF1:
                //retour une liste de string
                this.envoyerListPlayConf((Joueur)demande.getObject(),1);
                break;
            case Message.LISTE_PLAY_CONF2:
                //retour une liste de string
                this.envoyerListPlayConf((Joueur)demande.getObject(),2);
                break;
            case Message.LISTE_PLAY_CONF3:
                //retour une liste de string
                this.envoyerListPlayConf((Joueur)demande.getObject(),3);
                break;
            case Message.LISTE_PLAY_CONF4:
                //retour une liste de string
                this.envoyerListPlayConf((Joueur)demande.getObject(),4);
                break;
            case Message.LISTE_PLAY_CONF5:
                //retour une liste de string
                this.envoyerListPlayConf((Joueur)demande.getObject(),5);
                break;
            case Message.LISTE_PLAY_CONF6:
                //retour une liste de string
                this.envoyerListPlayConf((Joueur)demande.getObject(),6);
                break;
                
                
            //Fin du jeu
                
            case Message.OP_DEC:
                deconnexionClient((Joueur)demande.getObject());
                break;
            case Message.OP_FIN:
                this.calculScore(demande.getObject());
                break;
            case Protocol.CONN_KO:
                break;
            
        }
    }
    
    
    private void inscription(Joueur j)
    {
        if(j.inscrire()==true)
        {
            Joueur jr = this.listJoueur.getJoueur(j.getNumJoueur());
            System.out.println("num joueur anonyme "+j.getNumJoueur());
            try //cas où le joueur anonyme s'inscrit
            {
                jr.setIdJoueur(j.getIdJoueur());
                jr.setMotDePasse(j.getMotDePasse());
                
                //il faut savoir si la liste de partie est vide
                
                System.out.println("le joueur a autant de jeu :"+jr.getListPlay().size());
                if(jr.getListPlay().size()!=0)
                {
                    jr.addListPlay();
                }
                
                Message m = new Message(Message.M_OK,j.getNumJoueur());
                if(this.protocolServer.respond(m)!=CONN_ACK)
                {
                    System.out.println("echec connection client" );
                }
                
                System.out.println("inscription effectué");
                
            }
            catch(Exception e)
            { // cas ou c'est un joueur qui s'inscrit avant même d'avoir jouer
                int x = this.listJoueur.nouveauJoeur();
                this.listJoueur.getJoueur(x).setConfiguration(0);
                this.listJoueur.getJoueur(x).setIdJoueur(j.getIdJoueur());
                this.listJoueur.getJoueur(x).setMotDePasse(j.getMotDePasse());
                
                Message m = new Message(Message.M_OK,x);
                if(this.protocolServer.respond(m)!=CONN_ACK)
                {
                    System.out.println("echec connection client" );
                    this.listJoueur.removeJoeur(x);
                }
               
                System.out.println("inscription effectué");
            }
               
        }
        else
        {
            Message m = new Message(Message.M_OK,-1);
            if(this.protocolServer.respond(m)!=CONN_ACK)
            {
                System.out.println("le pseudo existe" );
            }
        }
    }
    
    
    private void connexion(Joueur j)
    {
        if(j.existJoueur()==true)
        {
            int x = this.listJoueur.nouveauJoeur();
            this.listJoueur.getJoueur(x).setConfiguration(0);
            this.listJoueur.getJoueur(x).setIdJoueur(j.getIdJoueur());
            this.listJoueur.getJoueur(x).setMotDePasse(j.getMotDePasse());

            Message m = new Message(Message.M_OK,x);
            if(this.protocolServer.respond(m)!=CONN_ACK)
            {
                System.out.println("echec connection client" );
                this.listJoueur.removeJoeur(x);
            }
        }
        //joueur n'existe pas ds db
        else
        {
            
            System.out.println("joeur inéxistant");
            Message m = new Message (Message.SESSION_FERME, null);
            this.protocolServer.respond(m);
            
        }
    }
    
    
    
    
    private void deconnexionClient(Joueur j ) 
    {
        this.supprimerJoeur(j);
        Message reponse = new Message(Message.M_OK,null);
        protocolServer.respond(reponse);
    }
    
    private void creerJoeur(int conf)
    {
        int x = this.listJoueur.nouveauJoeur();
        this.listJoueur.getJoueur(x).setConfiguration(conf);
        
        Message m = new Message(Message.M_OK,x);
        if(this.protocolServer.respond(m)!=CONN_ACK)
        {
            System.out.println("echec connection client" );
            this.listJoueur.removeJoeur(x);
        }
    }
    
    private void supprimerJoeur(Joueur j )
    {
        this.listJoueur.removeJoeur(j.getNumJoueur());
    }
    
    private void changerConfiguration(int conf , Joueur jr )
    {
        Joueur j = this.listJoueur.getJoueur(jr.getNumJoueur());
        if(j!=null)
        {
            j.setConfiguration(conf);
            j.setLastSessionTime();
            Message m = new Message(Message.M_OK,conf);
            this.protocolServer.respond(m);
        }
        else
        {
            System.out.println("joeur inéxistant");
            Message m = new Message (Message.SESSION_FERME, null);
            this.protocolServer.respond(m);
        }
    }
    
    private void envoyerListePlay(Joueur j )
    {
        Joueur jr = this.listJoueur.getJoueur(j.getNumJoueur());
        if(jr != null)
        {
            jr.setLastSessionTime();
            Message m = new Message(Message.M_OK,jr.getListPlay());
            this.protocolServer.respond(m);
        }
        else
        {
            System.out.println("joeur inéxistant");
            Message m = new Message (Message.SESSION_FERME, null);
            this.protocolServer.respond(m);
        }
    }
    
     private void envoyerBestSolution(Joueur j )
    {
        Joueur jr = this.listJoueur.getJoueur(j.getNumJoueur());
        if(jr != null)
        {
            jr.setLastSessionTime();
            Message m = new Message(Message.M_OK,jr.getBestSolution());
            this.protocolServer.respond(m);
        }
        else
        {
            System.out.println("joeur inéxistant");
            Message m = new Message (Message.SESSION_FERME, null);
            this.protocolServer.respond(m);
        }
    }
    
    
    private void creerJeu(Joueur j )
    {
       
        Joueur jr = this.listJoueur.getJoueur(j.getNumJoueur());
        if(jr != null)
           {
           nbJeu +=1;
           Config config= this.listJoueur.getJoueur(j.getNumJoueur()).getConfig();
           play = new Play(nbJeu , config);
           jr.setLastSessionTime();
           System.out.println("new Jeu:: time start="+play.getTimeStart());
           Message m = new Message(Message.M_OK,play);
           if(this.protocolServer.respond(m)==CONN_ACK)
           {
               /* on supprime les jeux qui n'ont pas été joué. 
                * le cas où le joueur demande un nouveau jeu, immédiatement après
                * avoir reçu un nouveau jeu.
                */
               if(jr.dernierJeuJouer() == false) 
                   jr.supprimerDernierJeu();
               jr.ajouterPlay(play);
               System.out.println("jeu ajouté numero : "+play.getNumJeu());
               System.out.println("nombre de jeu dans la liste du joueur " + jr.nbrJeu());
           }
        }
        else
        {
            System.out.println("joeur inéxistant");
            Message m = new Message (Message.SESSION_FERME, null);
            this.protocolServer.respond(m);
        }
        
    }
    
    private void aff(String msg) {
        System.out.println("SERVEUR : "+msg);
    }
    
    public boolean timeOut(long timeStart, long timeEnd , long tmp){
        return timeEnd>=(timeStart+tmp)+1000;
    }
    
    public int verifierResultat(int res , int but , int butPro)
    {
        int r = 0;
        
        if(butPro == res)
            r=50;
        if(but == res)
            r = 100;
        
        return r;
    }
    private void calculScore(Object ob) 
    {
        Resultat o= (Resultat)ob;
        o.setTimeEnd();
        int i = o.getJoueur().getNumJoueur();
        
        int score = 0;
        int butPropose = o.getButPropose();
        int resultat = o.getsolution().getResultat();
        Joueur jr = this.listJoueur.getJoueur(i);
       
        try{
           
            Play check = jr.getPlay();
            check.setSolution(o.getsolution());
            check.setButPropose(butPropose);
            long timeStart= check.getTimeStart();
            long timeEnd = Calendar.getInstance().getTimeInMillis();
            long duree = check.getDuree();
            
            if(!timeOut(timeStart,timeEnd,duree)){
                boolean x = true;
                But bt= new But(check.getButB().getBut());
                Play input= new Play();
                input.nouvellePartie(bt, check.getPlaque(), 0);
                x=check.checkSolution(o.getsolution());
               
                if(x==true)
                {
                    score = this.verifierResultat(resultat, bt.getBut(), butPropose);
                    this.protocolServer.respond(new Message(Message.OP_FIN,score));
                    //on sauvegarde la partie reussit
                    jr.setPlay(check);
                    if(jr.getIdJoueur()!="")
                        jr.addListPlay();
                    check.setMessage("bien jouer");
                    check.setScore(score);
                }
                else
                {
                    this.protocolServer.respond(new Message(Message.ERREUR_EXPRESSION,0));
                    check.setScore(0);
                    System.out.println("erreur solution");
                }

            }  
            else
            {
                this.protocolServer.respond(new Message(Message.TIMEOUT,0));
                check.setMessage("temps dépassé");
                check.setScore(score);
                     
                System.out.println("time out");
            }
            
        }
        catch(Exception e)
        {
            System.out.println("Joeur inexistant  "+e.toString());
            this.protocolServer.respond(new Message(Message.MAUVAIS_JEU,-1));
        }
           
    }  

    //list play config
    
    private void envoyerListPlayConf(Joueur joueur, int config) {
        
       Joueur jr = this.listJoueur.getJoueur(joueur.getNumJoueur());
        if(jr != null)
        {
            jr.setLastSessionTime();
            Message m = new Message(Message.M_OK,joueur.getListPlayConfig(config));
            this.protocolServer.respond(m);
        }
        else
        {
            System.out.println("joeur inéxistant");
            Message m = new Message (Message.SESSION_FERME, null);
            this.protocolServer.respond(m);
        }
    
    }

    private void envoyerListPlayDate(Joueur joueur) {
        Joueur jr = this.listJoueur.getJoueur(joueur.getNumJoueur());
        if(jr != null)
        {
            jr.setLastSessionTime();
            Message m = new Message(Message.M_OK,joueur.getListPlayIdSession());
            this.protocolServer.respond(m);
        }
        else
        {
            System.out.println("joeur inéxistant");
            Message m = new Message (Message.SESSION_FERME, null);
            this.protocolServer.respond(m);
        }
    }

    private void envoyerListSessionDate(Joueur joueur) {
        Joueur jr = this.listJoueur.getJoueur(joueur.getNumJoueur());
        if(jr != null)
        {
            jr.setLastSessionTime();
            Message m = new Message(Message.M_OK,joueur.getListPlaySessionDate());
            this.protocolServer.respond(m);
        }
        else
        {
            System.out.println("joeur inéxistant");
            Message m = new Message (Message.SESSION_FERME, null);
            this.protocolServer.respond(m);
        }
        
    }
    
   
}
