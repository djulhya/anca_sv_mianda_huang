/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package commun;

import java.io.BufferedReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;

/**
 *
 * @author xun
 */
public class Protocol {
    
    protected Socket socket;
    protected ObjectOutputStream outObject;
    protected ObjectInputStream inObject;
    protected Message msg_recu = null;
    protected String adrIP;
    protected int port;
    
    //Messages de connexions
    public static final int CONN_OK = 100;
    public static final int CONN_KO = 200;
    public static final int CONN_NOT_JEU = 400; 
    public static final int CONN_ACK = 500;
    

    protected void write(String s) {
        try{
            this.outObject.writeObject(s);
            this.outObject.flush();
        }
        catch(Exception e){}
    }

    protected void write(Message m) {
        try{
            this.outObject.writeObject(m);
            this.outObject.flush();
        }
        catch(Exception e){}
    }
    
}
